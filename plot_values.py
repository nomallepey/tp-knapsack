import matplotlib.pyplot as plt
import pandas as pd




def plot_time_LP_Greedy():
    time = pd.read_csv("knapsack.csv", sep=";")
    plt.figure()
    plt.plot(time['n'], time[' LP'], label='Relaxation Linéaire')
    plt.plot(time['n'], time[' Greedy'], label='Heuristique Gloutonne')
    plt.xlabel('n')
    plt.ylabel("Temps d'exécution (microsecondes)")
    plt.title("Temps d'exécution en fonction de la taille du problème")
    plt.legend()
    
    plt.savefig('temps_LP_Greedy.png')
    plt.close()

def plot_opti_gap():
    gap_opti = pd.read_csv("knapsackOpti.csv", sep=";", decimal=",")
    plt.figure()
    plt.plot(gap_opti['n'], gap_opti[' LP'], label='Relaxation Linéaire')
    plt.plot(gap_opti['n'], gap_opti[' Greedy'], label='Heuristique Gloutonne')
    plt.plot(gap_opti['n'], gap_opti[' Rounding'], label='Heuristique Arrondie')
    plt.xlabel('n')
    plt.ylabel("Diffèrences d'optimalité (pourcentages)")
    plt.title("Gap d'optimalité par rapport a la solution exacte \nen fonction de la taille du problème")
    plt.legend()

    plt.savefig('gap_opti.png')
    plt.close()

def plot_fixed_var():
    fixed = pd.read_csv("preprocVar.csv", sep=";")
    plt.figure()
    plt.plot(fixed['n'], fixed[' varFix'])
    plt.xlabel('n')
    plt.ylabel("Variables fixés (pourcentages)")
    plt.title("Pourcentages de variables fixés en fonction de la taille du problème")
    
    plt.savefig('fixed_var.png')
    plt.close()

def plot_time_preproc_exact():
    time = pd.read_csv("knapsack.csv", sep=";")
    plt.figure()
    plt.plot(time['n'], time[' PreIP'], label='Simplex + preprocessing')
    plt.plot(time['n'], time[' IP'], label='Simplex')
    plt.xlabel('n')
    plt.ylabel("Temps d'exécution (microsecondes)")
    plt.title("Temps d'exécution en fonction de la taille du problème")
    plt.legend()
    
    plt.savefig('temps_preproc_exact.png')
    plt.close()
#Temps en fonction de n (Greedyy, LP) O
#Gap d'opti lin, greedy et rounding
#Perc var fixed
#Temps en fonction de n (Preproc, Exact)
if __name__ == "__main__":
    plot_time_LP_Greedy()
    plot_opti_gap()
    plot_fixed_var()
    plot_time_preproc_exact()
