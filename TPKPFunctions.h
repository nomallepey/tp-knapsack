#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <strings.h>
#include <math.h>
#include <errno.h>
#include <time.h>
#include<ilcplex/cplex.h>
#include <assert.h>

//IP problem structure
typedef struct IP_problem
{
        //Internal structures
        CPXENVptr env;
        CPXLPptr lp;
	//number of variables
	int nv;
	//number of constraints
	int nc;

        //Output solution
        double *x;
        //Costs array
        double *cost;
        //TYpe of the variables (binary or continuous)
        char *c_type;
        //Bounds over the variables
        double *up_bound;
        double *low_bound;
	//Names of the variables
	char** var_name;

        //Right hand section of the constraints
        double *rhs;
        //Sense of the constraints
        char *sense;
        //Left hand section for data constraints
        int *rmatbeg;
        int *rmatind;
        double *rmatval;
	int nz;
	//Names of the constraints 
	char** const_name;
        //Solution status
        int solstat;
        double objval;
} IP_problem;

typedef struct dataSet 
{
	//Attributes of the instance
	//Nombre d'objets
	int n;
	//Capacite b
	int b;

	//Tableau d'entiers de taille n contenant la valeur de chacun des objets
	int*c;
	//Tableau d'entiers de taille n contenant le poids de chacun des objets
	int*a;

	//CPLEX problem data
	IP_problem master;

} dataSet;

typedef struct IdUPair
{
	int id;
	double utility;
}IdUPair;

int display_instance(dataSet* dsptr,int display_stats_only);
int read_TPKP_instance(FILE*fin,dataSet* dsptr, int display_stats_only);
int generate_random_instance(int n, dataSet* dsptr,int display_stats_only);
int free_KPdata(dataSet* dsptr);
int free_IPproblem(IP_problem* ipprobptr);
int display_KPsolution(dataSet* dsptr, double* x,int display_stats_only, double *percent);
IdUPair* TPKP_gen_sort_pairs(dataSet*dsptr);

//Solution methods
int TPKP_solve_exact(dataSet* dsptr, double*x);
int TPKP_solve_LP(dataSet* dsptr, double*x);
int TPKP_solve_greedy(dataSet* dsptr, double*x);
int TPKP_solve_rounding(dataSet* dsptr, double*x);
int TPKP_preprocessIP(dataSet* dsptr, double*x, double* y);
void initialiseAtZero(double *x, int n);

