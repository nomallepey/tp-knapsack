<?xml version = "1.0" encoding="UTF-8" standalone="yes"?>
<CPLEXSolution version="1.2">
 <header
   problemName="TPKP"
   solutionName="incumbent"
   solutionIndex="-1"
   objectiveValue="176"
   solutionTypeValue="3"
   solutionTypeString="primal"
   solutionStatusValue="101"
   solutionStatusString="integer optimal solution"
   solutionMethodString="mip"
   primalFeasible="1"
   dualFeasible="1"
   MIPNodes="0"
   MIPIterations="1"
   writeLevel="1"/>
 <quality
   epInt="1.0000000000000001e-05"
   epRHS="9.9999999999999995e-07"
   maxIntInfeas="0"
   maxPrimalInfeas="0"
   maxX="1"
   maxSlack="0"/>
 <linearConstraints>
  <constraint name="capacity_1" index="0" slack="0"/>
 </linearConstraints>
 <variables>
  <variable name="x_j0" index="0" value="1"/>
  <variable name="x_j1" index="1" value="1"/>
  <variable name="x_j2" index="2" value="0"/>
  <variable name="x_j3" index="3" value="0"/>
  <variable name="x_j4" index="4" value="0"/>
  <variable name="x_j5" index="5" value="0"/>
  <variable name="x_j6" index="6" value="1"/>
  <variable name="x_j7" index="7" value="1"/>
  <variable name="x_j8" index="8" value="0"/>
  <variable name="x_j9" index="9" value="0"/>
  <variable name="x_j10" index="10" value="0"/>
  <variable name="x_j11" index="11" value="0"/>
  <variable name="x_j12" index="12" value="0"/>
  <variable name="x_j13" index="13" value="1"/>
  <variable name="x_j14" index="14" value="1"/>
  <variable name="x_j15" index="15" value="0"/>
  <variable name="x_j16" index="16" value="0"/>
  <variable name="x_j17" index="17" value="0"/>
  <variable name="x_j18" index="18" value="1"/>
  <variable name="x_j19" index="19" value="1"/>
  <variable name="x_j20" index="20" value="0"/>
  <variable name="x_j21" index="21" value="1"/>
  <variable name="x_j22" index="22" value="1"/>
  <variable name="x_j23" index="23" value="1"/>
  <variable name="x_j24" index="24" value="0"/>
  <variable name="x_j25" index="25" value="0"/>
  <variable name="x_j26" index="26" value="0"/>
  <variable name="x_j27" index="27" value="0"/>
  <variable name="x_j28" index="28" value="1"/>
  <variable name="x_j29" index="29" value="1"/>
  <variable name="x_j30" index="30" value="1"/>
  <variable name="x_j31" index="31" value="1"/>
  <variable name="x_j32" index="32" value="0"/>
  <variable name="x_j33" index="33" value="0"/>
  <variable name="x_j34" index="34" value="0"/>
  <variable name="x_j35" index="35" value="0"/>
  <variable name="x_j36" index="36" value="1"/>
  <variable name="x_j37" index="37" value="0"/>
  <variable name="x_j38" index="38" value="1"/>
  <variable name="x_j39" index="39" value="0"/>
  <variable name="x_j40" index="40" value="0"/>
  <variable name="x_j41" index="41" value="0"/>
  <variable name="x_j42" index="42" value="1"/>
  <variable name="x_j43" index="43" value="0"/>
  <variable name="x_j44" index="44" value="1"/>
  <variable name="x_j45" index="45" value="0"/>
  <variable name="x_j46" index="46" value="0"/>
  <variable name="x_j47" index="47" value="1"/>
  <variable name="x_j48" index="48" value="0"/>
  <variable name="x_j49" index="49" value="0"/>
  <variable name="x_j50" index="50" value="0"/>
  <variable name="x_j51" index="51" value="0"/>
  <variable name="x_j52" index="52" value="0"/>
  <variable name="x_j53" index="53" value="1"/>
  <variable name="x_j54" index="54" value="1"/>
  <variable name="x_j55" index="55" value="0"/>
  <variable name="x_j56" index="56" value="1"/>
  <variable name="x_j57" index="57" value="1"/>
  <variable name="x_j58" index="58" value="0"/>
  <variable name="x_j59" index="59" value="1"/>
  <variable name="x_j60" index="60" value="1"/>
  <variable name="x_j61" index="61" value="0"/>
  <variable name="x_j62" index="62" value="0"/>
  <variable name="x_j63" index="63" value="1"/>
  <variable name="x_j64" index="64" value="1"/>
  <variable name="x_j65" index="65" value="0"/>
  <variable name="x_j66" index="66" value="1"/>
  <variable name="x_j67" index="67" value="0"/>
  <variable name="x_j68" index="68" value="1"/>
 </variables>
 <objectiveValues>
  <objective index="0" name="" value="176"/>
 </objectiveValues>
</CPLEXSolution>
