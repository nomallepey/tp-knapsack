#include <ctype.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
#include <math.h>
#include "TPKPFunctions.h"
#include<ilcplex/cplex.h>


int TPKP(int n, FILE *preprocVar, FILE *knapsack, FILE * knapsackOpti)
{
	int rval =0;	
	dataSet data;
	//Generate instance or read it
	int display_stats_only=1;

	double roundingTime, greedyTime, LPTime, IPTime, PreIPTime;

	generate_random_instance(n,&data,display_stats_only);

	clock_t start, end;

	//rounding solution
	fprintf(stderr,"____________________\n");
	fprintf(stderr," Rounding the LP sol\n");
	fprintf(stderr,"____________________\n");
	double*rounding = (double*)malloc(sizeof(double)*n);
	double rounding_percent;
	initialiseAtZero(rounding,data.n);
	start = clock();
	rval = TPKP_solve_rounding(&data,rounding);
	end = clock();
	if(rval){fprintf(stderr,"Error while in TPKP_solve_rounding.\n"); exit(0);}
	//We display the solution
	display_KPsolution(&data, rounding,display_stats_only, &rounding_percent);
	free(rounding);
	roundingTime = ((double) (end - start)) / CLOCKS_PER_SEC;



	//Greedy solution
	fprintf(stderr,"____________________\n");
	fprintf(stderr,"     Greedy IP\n");
	fprintf(stderr,"____________________\n");
	double*greedy = (double*)malloc(sizeof(double)*n);
	double greedy_percent;
	initialiseAtZero(greedy, data.n);
	start = clock();
	rval = TPKP_solve_greedy(&data,greedy);
	end = clock();
	if(rval){fprintf(stderr,"Error while in TPKP_solve_greedy.\n"); exit(0);}
	//We display the solution
	display_KPsolution(&data, greedy,display_stats_only, &greedy_percent);
	free(greedy);
	greedyTime = ((double) (end - start)) / CLOCKS_PER_SEC;


	//Exact solution LP
	fprintf(stderr,"____________________\n");
	fprintf(stderr,"  Solving the LP\n");
	fprintf(stderr,"____________________\n");
	double*exact_lp = (double*)malloc(sizeof(double)*n);
	double lp_percent;
	initialiseAtZero(exact_lp, data.n);
	start = clock();
	rval = TPKP_solve_LP(&data,exact_lp);
	end = clock();
	if(rval){fprintf(stderr,"Error while in TPKP_solve_LP.\n"); exit(0);}
	//We display the solution
	display_KPsolution(&data, exact_lp,display_stats_only, &lp_percent);
	free(exact_lp);
	LPTime = ((double) (end - start)) / CLOCKS_PER_SEC;

	//Exact solution
	fprintf(stderr,"____________________\n");
	fprintf(stderr,"  Solving the IP\n");
	fprintf(stderr,"____________________\n");
	double*exact_ip = (double*)malloc(sizeof(double)*n);
	double exact_percent;
	start = clock();
	rval = TPKP_solve_exact(&data, exact_ip);
	end = clock();
	if(rval){fprintf(stderr,"Error while in TPKP_solve_exact.\n"); exit(0);}
	//We display the solution
	display_KPsolution(&data, exact_ip,display_stats_only, &exact_percent);
	free(exact_ip);
	IPTime = ((double) (end - start)) / CLOCKS_PER_SEC;

	//Preprocessing + IP
	fprintf(stderr,"____________________\n");
	fprintf(stderr,"Preprocessing the IP\n");
	fprintf(stderr,"____________________\n");
	double*preproc_ip = (double*)malloc(sizeof(double)*n);
	double percent_preproc;
	start = clock();
	rval = TPKP_preprocessIP(&data, preproc_ip, &percent_preproc);
	end = clock();
	if(rval){fprintf(stderr,"Error while in TPKP_preprocessIP.\n"); exit(0);}
	//We display the solution
	display_KPsolution(&data, preproc_ip,display_stats_only, NULL);
	free(preproc_ip);
	PreIPTime = ((double) (end - start)) / CLOCKS_PER_SEC;

	//Freeings
	rval = free_KPdata(&data);
	if(rval){fprintf(stderr,"Error while in free_KPdata.\n"); exit(0);}

	fprintf(knapsack,"%d; %f; %f; %f; %f; %f\n", n, roundingTime*1000000, greedyTime*1000000, LPTime*1000000, IPTime*1000000, PreIPTime*1000000);
	fprintf(knapsackOpti,"%d; %f; %f; %f\n", n, lp_percent, greedy_percent,rounding_percent);
	fprintf(preprocVar,"%d; %f\n", n, percent_preproc);
	return rval;
 

}
int main(int argc, char **argv){
	FILE *preprocVar;
	FILE *knapsack;
	FILE *knapsackOpti;

	knapsack = fopen("knapsack.csv", "w+");
	fprintf(knapsack,"n; Rounding; Greedy; LP; IP; PreIP\n");
	
	preprocVar = fopen("preprocVar.csv", "w+");
	fprintf(preprocVar,"n; varFix;\n");

	knapsackOpti = fopen("knapsackOpti.csv", "w+");
	fprintf(knapsackOpti,"n; LP; Greedy; Rounding;\n");

	// création des graphiques
	int list_n[] = {10,20,50,100, 200, 400, 600, 800, 1000};
	for (int i = 0; i<sizeof(list_n)/sizeof(list_n[0]); i++){
		TPKP(list_n[i], preprocVar, knapsack, knapsackOpti);

	}
	fclose(preprocVar);
	fclose(knapsack);
	fclose(knapsackOpti);
	system("python3 plot_values.py");
}

