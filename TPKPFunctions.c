#include "TPKPFunctions.h"
#include <math.h>
#include <stdlib.h>
#include <stdbool.h>
#include <sys/time.h>
#include<stdio.h>
#include<ilcplex/cplex.h>


int display_instance(dataSet* dsptr,int display_stats_only)
{
	int rval = 0;

	int b = dsptr->b;
	int n = dsptr->n;

	int i;

	fprintf(stderr,"\nWe have capacity b = %d and there is %d items of values/weights:\n",
			b,n);
	if(display_stats_only)
		return rval;
	fprintf(stderr,"i\tci\tai\tu_i\n");
	fprintf(stderr,"--------------------------\n");


	for( i = 0 ; i < n ; i++)
		fprintf(stderr,"%d\t%d\t%d\t%.2lf\n",i,dsptr->c[i],dsptr->a[i],(double)dsptr->c[i]/dsptr->a[i]);
	fprintf(stderr,"\n");



	return rval;
}



int read_TPKP_instance(FILE*fin,dataSet* dsptr,int display_stats_only)
{
	int rval = 0;

	//capacites b et g
	int b;
	//Nombre d'objets
	int n;
	int rvalfscanf=0;
	rvalfscanf=fscanf(fin,"%d,%d\n",&n,&b);
	if(rvalfscanf<0)
	{
		fprintf(stderr,"fscanf failed\n");
		exit(0);
	}

	dsptr->b = b;

	dsptr->n = n;
	dsptr->c = (int*)malloc(sizeof(int)*n);
	dsptr->a = (int*)malloc(sizeof(int)*n);


	int i;
	for( i = 0 ; i < n ; i++)
	{
		rvalfscanf=fscanf(fin,"%d,%d\n",&(dsptr->c[i]),&(dsptr->a[i]));
		if(rvalfscanf<0)
		{
			fprintf(stderr,"fscanf failed\n");
			exit(0);
		}
	}

	fprintf(stderr,"\nInstance file read\n");
	display_instance(dsptr,display_stats_only);

	return rval;
}

int generate_random_instance(int n, dataSet* dsptr,int display_stats_only)
{
	int rval = 0;

	dsptr->n = n;
	dsptr->c = (int*)malloc(sizeof(int)*n);
	dsptr->a = (int*)malloc(sizeof(int)*n);


	int i;
	int suma = 0;
	for( i = 0 ; i < n ; i++)
	{	
		dsptr->c[i]=1+rand()%10;
		dsptr->a[i]=1+rand()%50;
		suma += dsptr->a[i];
	}

	int b = 0.2*suma;
	dsptr->b = b;

	fprintf(stderr,"\nInstance generated\n");
	display_instance(dsptr,display_stats_only);

	return rval;
}


int TPKP_compute_objective(dataSet*dsptr, double*x, double* objval)
{
	int rval = 0;

	int n = dsptr->n;
	int* c = dsptr->c;
	(*objval) = 0;
	for( int j = 0 ; j < n ; j++)
		(*objval)+=x[j]*c[j];

	return rval;
}



int free_IPproblem(IP_problem* ipprobptr)
{
	int rval = 0;

        //Internal structures
	rval = CPXfreeprob (ipprobptr->env, &(ipprobptr->lp));
	if(rval)
	{
		fprintf (stderr, "CPXfreeprob failed, error code %d.\n", rval);
		exit(0);
	}
	rval = CPXcloseCPLEX(&(ipprobptr->env));
	if(rval)
	{
		fprintf (stderr, "CPXcloseCPLEX failed, error code %d.\n", rval);
		exit(0);
	}
        //Output solution
        free(ipprobptr->x);
        //Costs array
        free(ipprobptr->cost);
        //TYpe of the variables (binary or continuous)
        free(ipprobptr->c_type);
        //Bounds over the variables
       	free(ipprobptr->up_bound);
        free(ipprobptr->low_bound);
	//Names of the variables
	int nv = ipprobptr->nv;
	for(int v = 0 ; v < nv ; v++)
		free(ipprobptr->var_name[v]);
	free(ipprobptr->var_name);

        //Right hand section of the constraints
        free(ipprobptr->rhs);
        //Sense of the constraints
        free(ipprobptr->sense);
        //Left hand section for data constraints
        free(ipprobptr->rmatbeg);
        free(ipprobptr->rmatind);
        free(ipprobptr->rmatval);
	//Names of the constraints 
	int nc = ipprobptr->nc;
	for(int c = 0 ; c < nc ; c++)
		free(ipprobptr->const_name[c]);
	free(ipprobptr->const_name);



	return rval;
}




int free_KPdata(dataSet* dsptr)
{
	int rval = 0;

	free(dsptr->c);
	free(dsptr->a);
	
	rval =free_IPproblem(&(dsptr->master));
	if(rval)
	{
		fprintf(stderr,"error while freeing IP_problem structure\n");
		exit(0);
	}
 	return rval;
}


int display_KPsolution(dataSet* dsptr, double* x,int display_stats_only, double* percent)
{
	int rval = 0;

	int j;
	int n = dsptr->n;
	int* c = dsptr->c;
	int* a = dsptr->a;
	int b = dsptr->b;

	double tol = 0.0001;

	//Solution stats
	double remaining_cap = b;
	double cost = 0;
	double total_cost = 0;
	int is_integer = 1;
	for( j = 0 ; j < n ; j ++)
	{
		if(	(x[j]<-tol) ||
			(x[j]>1+tol) ||
			( (x[j]>tol) && (x[j] < 1-tol)))
			is_integer = 0;

		remaining_cap -= x[j]*a[j];
		cost += x[j]*c[j];
		total_cost += c[j];

	}
	double frac_cap = (b-remaining_cap)/(double)b*100.;
	double frac_cost = cost/total_cost*100.;

	if(is_integer)
		fprintf(stderr,"The current solution is integer\n");
	else	
		fprintf(stderr,"The current solution is fractional\n");

	fprintf(stderr,"Uses %.2lf (%.2lf%%) and has objective %.2lf (%.2lf%%) \n",
			b-remaining_cap,frac_cap,
			cost,frac_cost
			);
	
	if (percent != NULL)
	{
		*percent = frac_cost;
	}

	if(display_stats_only)
		return rval;
	for( j = 0 ; j < n ; j ++)
		fprintf(stderr,"x[%d] = %.2lf\n",j,x[j]);

	return rval;
}

int TPKP_solve_exact(dataSet* dsptr, double*x)
{
	int rval = 0;

	IP_problem* ip_prob_ptr = &(dsptr->master);
	ip_prob_ptr->env = NULL;
	ip_prob_ptr->lp = NULL;
	ip_prob_ptr->env = CPXopenCPLEX (&rval);
	if(rval) fprintf(stderr,"ERROR WHILE CALLING CPXopenCPLEX\n");
	if ( ip_prob_ptr->env == NULL ) 
	{
		char  errmsg[1024];
		fprintf (stderr, "Could not open CPLEX environment.\n");
		CPXgeterrorstring (ip_prob_ptr->env, rval, errmsg);
		fprintf (stderr, "%s", errmsg);
		exit(0);	
	}

	//We create the MIP problem
	ip_prob_ptr->lp = CPXcreateprob (ip_prob_ptr->env, &rval, "TPKP");
	if(rval) fprintf(stderr,"ERROR WHILE CALLING CPXcreateprob\n");

	rval = CPXsetintparam (ip_prob_ptr->env, CPX_PARAM_DATACHECK, CPX_ON); 
	//rval = CPXsetintparam (ip_prob_ptr->env, CPX_PARAM_SCRIND, CPX_ON);

	int n = dsptr->n;
	int* a = dsptr->a;
	int* c = dsptr->c;
	int b = dsptr->b;

	//Number of variables
	int nv = n;

	//Number of constraints
	int nc = 1;
	ip_prob_ptr->nc = nc;


	//We fill our arrays
	//Memory
	ip_prob_ptr->nv = nv;
        ip_prob_ptr->x = (double*)malloc(sizeof(double)*nv);
        ip_prob_ptr->cost = (double*)malloc(sizeof(double)*nv);
        ip_prob_ptr->c_type = (char*)malloc(sizeof(char)*nv);
        ip_prob_ptr->up_bound = (double*)malloc(sizeof(double)*nv);
        ip_prob_ptr->low_bound = (double*)malloc(sizeof(double)*nv);
	ip_prob_ptr->var_name = (char**)malloc(sizeof(char*)*nv);

	int j,id = 0;
	//Structures keeping the index of each variable
	int*id_x_j = (int*)malloc(sizeof(int)*n);

	//variables xi definition
	for( j = 0 ; j < n ; j++)
	{
		//We keep the id
		id_x_j[j] = id;

		//We generate the variable attributes
		ip_prob_ptr->x[id] = 0;
		ip_prob_ptr->cost[id] = c[j];
		ip_prob_ptr->c_type[id] = 'B';
		ip_prob_ptr->up_bound[id] = 1;
		ip_prob_ptr->low_bound[id] = 0;
		ip_prob_ptr->var_name[id] = (char*)malloc(sizeof(char)*1024);
	        snprintf(       ip_prob_ptr->var_name[id],
        	                1024,
                	        "x_j%d",
                        	j);
		id++;
	}


	rval = CPXnewcols( ip_prob_ptr->env, ip_prob_ptr->lp, 
			nv, 
			ip_prob_ptr->cost, 
			ip_prob_ptr->low_bound,
			ip_prob_ptr->up_bound,
			ip_prob_ptr->c_type,
			ip_prob_ptr->var_name);
	if(rval)
		fprintf(stderr,"CPXnewcols returned errcode %d\n",rval);

	//Constraints part
        ip_prob_ptr->rhs = (double*)malloc(sizeof(double));
        ip_prob_ptr->sense = (char*)malloc(sizeof(char));
        ip_prob_ptr->rmatbeg = (int*)malloc(sizeof(int));
	ip_prob_ptr->nz = n;


        ip_prob_ptr->rmatind = (int*)malloc(sizeof(int)*nv);
        ip_prob_ptr->rmatval = (double*)malloc(sizeof(double)*nv);
	ip_prob_ptr->const_name = (char**)malloc(sizeof(char*));
	ip_prob_ptr->const_name[0] = (char*)malloc(sizeof(char)*1024);

	//We fill what we can 
	ip_prob_ptr->rmatbeg[0] = 0;

	//We generate and add each constraint to the model

	//capacity constraint #1
	ip_prob_ptr->rhs[0] = b;
	ip_prob_ptr->sense[0] = 'L';
	//Constraint name
	snprintf(       ip_prob_ptr->const_name[0],
			1024,
			"capacity_1"
			);
	id=0;
	//variables x_j coefficients
	for( j = 0 ; j < n ; j++)
	{
		ip_prob_ptr->rmatind[id] = id_x_j[j];
		ip_prob_ptr->rmatval[id] =  a[j];
		id++;
	}
	rval = CPXaddrows( ip_prob_ptr->env, ip_prob_ptr->lp, 
			0,//No new column
			1,//One new row
			n,//Number of nonzero coefficients
			ip_prob_ptr->rhs, 
			ip_prob_ptr->sense, 
			ip_prob_ptr->rmatbeg, 
			ip_prob_ptr->rmatind, 
			ip_prob_ptr->rmatval,
			NULL,//No new column
			ip_prob_ptr->const_name );
	if(rval)
		fprintf(stderr,"CPXaddrows returned errcode %d\n",rval);


	//We switch to maximization
	rval = CPXchgobjsen( ip_prob_ptr->env, ip_prob_ptr->lp, CPX_MAX );


	//We write the problem for debugging purposes, can be commented afterwards
	rval = CPXwriteprob (ip_prob_ptr->env, ip_prob_ptr->lp, "multiKnapsack.lp", NULL);
	if(rval)
		fprintf(stderr,"CPXwriteprob returned errcode %d\n",rval);

	//We solve with extreme optimality tolerance
	//rval = CPXsetdblparam (ip_prob_ptr->env, CPX_PARAM_EPOPT, 0.000000001); 
	//if(rval)
	//	fprintf(stderr,"CPXwriteprob returned errcode %d\n",rval);

	//We solve the model
	rval = CPXmipopt (ip_prob_ptr->env, ip_prob_ptr->lp);
	if(rval)
		fprintf(stderr,"CPXmipopt returned errcode %d\n",rval);

	rval = CPXsolwrite( ip_prob_ptr->env, ip_prob_ptr->lp, "multiKnapsack.sol" );
	if(rval)
		fprintf(stderr,"CPXsolwrite returned errcode %d\n",rval);

	//We get the objective value
	rval = CPXgetobjval( ip_prob_ptr->env, ip_prob_ptr->lp, &(ip_prob_ptr->objval) );
	if(rval)
		fprintf(stderr,"CPXgetobjval returned errcode %d\n",rval);

	//We get the best solution found 
	rval = CPXgetx( ip_prob_ptr->env, ip_prob_ptr->lp, ip_prob_ptr->x, 0, nv-1 );
	if(rval)
		fprintf(stderr,"CPXgetx returned errcode %d\n",rval);

	for( j = 0 ; j < n ; j++)
		x[j] = ip_prob_ptr->x[j];

	free(id_x_j);

	return rval;
}



int IdUPairComparator ( const void * first, const void * second ) {
	double firstU = ((const IdUPair *) first)->utility;
	double secondU =((const IdUPair *) second)->utility;
	if(firstU > secondU)
		return -1;
	else return 1;
}

IdUPair* TPKP_gen_sort_pairs(dataSet*dsptr)
{
	int n = dsptr->n;
	//We create the array of pairs id/utility
	IdUPair* ratio = (IdUPair*)malloc(n*sizeof(IdUPair));
	for(int i=0;i<dsptr->n;i++)
	{
		ratio[i].id = i;
		ratio[i].utility = (double)(dsptr->c[i])/dsptr->a[i];
	}

	qsort(ratio, dsptr->n, sizeof(ratio[0]), IdUPairComparator);

	return ratio;
}


int TPKP_solve_LP(dataSet* dsptr, double*x)
{
	int rval = 0;
	int b = dsptr->b;
	int n = dsptr->n;

	IdUPair* pairs = TPKP_gen_sort_pairs(dsptr);

	for (int j = 0; j < n; j++){
		if (b <= 0) return rval;
		double v = (double) b/dsptr->a[pairs[j].id];
		x[pairs[j].id] = (double)v < 1?(double) v : 1;
		b -= dsptr->a[pairs[j].id];
	}

	return rval;
}

void initialiseAtZero(double*x, int n){
	for (int j = 0; j < n; j++)
		x[j] = 0;
}

int TPKP_solve_rounding(dataSet* dsptr, double*x)
{
	int rval = 0;
	int b = dsptr->b;
	int n = dsptr->n;

	IdUPair* pairs = TPKP_gen_sort_pairs(dsptr);
	
	for (int j = 0; j < n; j++)
	{
		if (b <= 0 || b/dsptr->a[pairs[j].id] < 1)
			return rval;
		x[pairs[j].id] =  1;
		b -= dsptr->a[pairs[j].id];
	}
	return rval;
}



int TPKP_solve_greedy(dataSet* dsptr, double*x)
{
	int rval = 0;
	int b = dsptr->b;
	int n = dsptr->n;

	IdUPair* pairs = TPKP_gen_sort_pairs(dsptr);
	for (int j = 0; j < n; j++){
		if (b <= 0) return rval;
		if (b >= dsptr->a[pairs[j].id]){
			x[pairs[j].id] = 1;
			b -= dsptr->a[pairs[j].id];
		}
	}
	return rval;
}

int findK(double* x, dataSet* dsptr){

	IdUPair* pairs = TPKP_gen_sort_pairs(dsptr);

	for (int i = 0; i<dsptr->n; i++)
	{
		if (x[pairs[i].id]==0) return i;
	}
	return -1;
}


int TPKP_preprocessIP(dataSet* dsptr, double*x, double* percent_var_fixed)
{
	int rval = 0;
	int b = dsptr->b;
	int n = dsptr->n;
	//int *c = malloc(sizeof(int)*n);
	double c;
	double w_poss = 0;
	double w_opti = 0;
	//memcpy(c, dsptr->c, sizeof(int)*n);
	double* x_poss = (double*)malloc(sizeof(double)*n);
	double* x_opti = (double*)malloc(sizeof(double)*n);
	initialiseAtZero(x_poss,n);
	initialiseAtZero(x_opti,n);
	int preproN = 0;
	
	TPKP_solve_greedy(dsptr, x_poss);
	TPKP_solve_LP(dsptr,x_opti);
    int k = findK(x_opti,dsptr);
	for (int i = 0; i < n; i++){
		w_poss += x_poss[i]*dsptr->c[i];
		w_opti += x_opti[i]*dsptr->c[i];
	}

	IdUPair* pairs = TPKP_gen_sort_pairs(dsptr);
	int n_var_fixed = 0;
	for (int j = 0; j < n; j++){
		c = dsptr-> c[pairs[j].id];
		c = fabs(c - ((double)dsptr->c[pairs[k].id]/dsptr->a[pairs[k].id] * dsptr->a[pairs[j].id]));
		//printf("c[%d] : %f\n", pairs[j].id, c);
		if (c >= w_opti - w_poss){
			if( j <= k - 1){
				x[pairs[j].id] = 1;
				b = b - dsptr->a[pairs[j].id];
			}
			else{
				x[pairs[j].id] = 0;
			}
			n_var_fixed++;
		}
		else
		{
			x[pairs[j].id] = -1;
			preproN += 1;
		}
		//printf("set var : %d to %f\n",pairs[j].id, x[pairs[j].id]);
	}

	*percent_var_fixed = 100 * n_var_fixed / n;
	
	//printf("k------>%d\nk-ieme : %d",k, pairs[k].id);
	dataSet preproDataset;
	preproDataset.n = preproN;
	preproDataset.c = (int*)malloc(sizeof(int)*preproN);
	preproDataset.a = (int*)malloc(sizeof(int)*preproN);
	preproDataset.b = b;

	int index = 0;
	for(int i = 0; i < n; i++)
	{
		if(x[i] == -1)
		{
			preproDataset.c[index] = dsptr->c[i];
			preproDataset.a[index] = dsptr->a[i];
			
			index++;
		}
	}

	display_instance(&preproDataset, 0);

	double* preproX = (double*)malloc(sizeof(double)*preproN);
	TPKP_solve_exact(&preproDataset, preproX);

	index = 0;
	for(int i = 0; i < n; i++)
	{
		if(x[i] == -1)
		{
			x[i] = preproX[index];
			index++;
		}
	}
	return rval;
}

